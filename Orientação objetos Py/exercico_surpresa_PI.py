class Escolaridade:

    def __init__(self, descricao):
        self.descricao = descricao

    def getDescricao(self):
        return self.descricao

class Pessoa:

    def __init__(self,escolaridade,naturalidade):
        self.escolaridade = escolaridade
        self.naturalidade = naturalidade

    def getEscolaridade(self):
        return self.escolaridade

    def getDescricaoEscolaridade(self):
        return self.escolaridade.getDescricao()

class Professor(Pessoa):

    def __init__(self, escolaridade, naturalidade):
        Pessoa.__init__(self,escolaridade,naturalidade)

    def getCidadeNaturalizada(self):
        return self.naturalidade

    def getTipoEnsino(self):
        return self.tipo.getDescricaoensino

class Curso:

    def __init__(self,coordenador,tipo):
        self.coordenador = coordenador
        self.tipo = tipo

    def getNomeCoordenador(self):
        return self.coordenador

    def getEscolaridadeCoordenador(self):
        return self.coordenador.getDescricaoEscolaridade()

    def getTipoEnsino(self):
        return self.tipo.getDescricaoensino


class Escola:

    def __init__(self,diretor):
        self.diretor = diretor

    def getEscolaridadeDiretor(self):
        return self.diretor.getDescricaoEscolaridade()

    def getNomeDiretor(self):
        return self.diretor


class Aluno(Pessoa):

    def __init__(self,escolaridade,naturalidade):
        Pessoa.__init__(self,escolaridade, naturalidade)

    def getEstadoNaturalizado(self):
        return self.naturalidade

    def getEstadoEstudando(self):
        return self.naturalidade

class Cidade:

    def __init__(self,cidade):
        self.cidade = cidade

class Estado:

    def __init__(self,estado):
        self.estado = estado

class TipoEnsino:

    def __init__(self,ensino):
        self.ensino = ensino

    def getDescricaoEnsino(self):
        return self.ensino

#Letra A
escolaridade = Escolaridade("Nivel superior")
professor = Professor(escolaridade,"")
print(f"Qual a escolaridade de um professor ? = {professor.getDescricaoEscolaridade()}")

#Letra B
escolaridade = Escolaridade("Pós Graduação")
professor = Professor(escolaridade,"")
curso = Curso(professor,"")
print(f"Qual a escolaridade do coordenador de um curso ? = {curso.getEscolaridadeCoordenador()}")

#Letra C
escolaridade = Escolaridade("Mestrado")
professor = Professor(escolaridade ,"")
escola = Escola(professor)
print(f"Qual a escolaridade do coordenador de uma escola ? = {escola.getEscolaridadeDiretor()}")

#Letra D
aluno = Aluno("", "RS")
estado =Estado(aluno)
cidade = Cidade(estado)
print(f"Qual o estado de naturalidade de um aluno ? = {aluno.getEstadoNaturalizado()}")

#Letra E
professor = Professor("","juiz De Fora",)
print(f"Qual cidade de nascimento de um professor ? = {professor.getCidadeNaturalizada()}")

#Letra F
aluno = Aluno("", "MG")
estado =Estado(aluno)
cidade = Cidade(estado)
print(f"Qual estado em que um aluno estuda ? = {aluno.getEstadoEstudando()}")

#Letra G
tipoensino= TipoEnsino("Ensino Superior")
curso = Curso("","")
professor = Professor(curso,"")
print(f"Qual o tipo de ensino que um professor foi contratado para lecionar ? = {tipoensino.getDescricaoEnsino()}")

#Letra H
professor = Professor("","")
curso = Curso("Darlon","")
aluno =Aluno(curso,"")
print(f"Quem é o coordenador do curso de um aluno ? = {curso.getNomeCoordenador()}")

#Letra I
escola = Escola("Alberto")
professor = Professor(escola,"")
print(f"Quem é o diretor de um professor ? = {escola.getNomeDiretor()}")

#Letra J
curso = Curso("Daves", "")
professor = Professor(curso,"")
print(f"Quem é o coordenador de um professor ? = {curso.getNomeCoordenador()}")