class Pessoa:

    def __init__(self, nome):
        self.nome = nome

    def getNome(self):
        return self.nome

    def setNome(self, nome):
        self.nome = nome


class PessoaFisica(Pessoa):

    def __init__(self, nome, cpf):
        Pessoa.__init__(self, nome)
        self.cpf = cpf

    def getCpf(self):
        return self.cpf

    def setCpf(self, cpf):
        self.cpf = cpf

class PessoaJuridica(Pessoa):
    def __init__(self,nome,cnpj):
        Pessoa.__init__(self,nome)
        self.cnpj = cnpj

    def getCnpj(self):
            return self.cnpj

    def setCnpj(self, cnpj):
            self.cnpj = cnpj

class Fornecedor(PessoaJuridica):
    def __init__(self,nome,cnpj,representante):
        PessoaJuridica.__init__(self,nome,cnpj)
        self.representante = representante

    def getRepresentante(self):
        return self.representante
    def setRepresentante(self,representante):
        self.representante = representante

class Cliente(PessoaFisica):

    def __init__(self, nome, cpf, limiteCredito):
        PessoaFisica.__init__(self, nome, cpf)
        self.limiteCredito = limiteCredito

    def getLimiteCredito(self):
        return self.limiteCredito

    def setLimiteCredito(self, limiteCredito):
        self.limiteCredito = limiteCredito

class Funcionario(PessoaFisica):

    def __init__(self, nome, cpf, salario):
        PessoaFisica.__init__(self, nome, cpf)
        self.salario = salario
    def getSalario(self):
        return self.salario
    def setSalario(self, salario):
        self.salario = salario

# nome = input("Digite um nome: ")
# cpf = input("Digite um cpf: ")
# limiteCredito = input("Digite um limite de credito: ")

# cliente = Cliente(nome, cpf, limiteCredito)

cliente = Cliente("Ana", 123, 1000)

print(f"Nome = {cliente.getNome()}")
print(f"CPF = {cliente.getCpf()}")
print(f"Limite de credito = {cliente.getLimiteCredito()}")

# nome = input("Digite um nome: ")
# cpf = input("Digite um cpf: ")
# salario = input("Digite um salario: ")

# funcionario = Funcionario(nome, cpf,salario)

funcionario = Funcionario("Emmy", 342, 1200)

print(f"Nome = {funcionario.getNome()}")
print(f"CNPJ = {funcionario.getCpf()}")
print(f"salario = {funcionario.getSalario()}")

# nome = input("Digite um nome: ")
# cnpj = input("Digite um cnpj: ")
# representante = input("Digite o nome do representante: ")

# fornecedor = fornecedor(nome, cnpj, representante)

fornecedor = Fornecedor("lac", 3334, "Alan")

print(f"Nome = {fornecedor.getNome()}")
print(f"CNPJ = {fornecedor.getCnpj()}")
print(f"o representante é = {fornecedor.getRepresentante()}")