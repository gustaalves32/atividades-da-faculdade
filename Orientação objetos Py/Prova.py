class Estado:
    def __init__(self,estado):
        self.estado = estado
    def getEstado(self): 
        return self.estado

class Escolaridade:
    def __init__(self,descricão):
        self.descricão = descricão

    def getDescricão(self):
        return self.descricão

    def setDescricão(self,descricão):
        self.descricão = descricão
class Pessoa:
    def __init__(self, escolaridade ,naturalidade):
        self.escolaridade = escolaridade
        self.naturalidade = naturalidade  

    def getEscolaridade(self):
        return self.escolaridade        

    def setEscolaridade(self,escolaridade):
        self.escolaridade = escolaridade

    def getDescricãoEscolaridade(self):
        return self.escolaridade.getDescricão()

    def getNaturalidade(self):
        return self.naturalidade

    def setNaturalidade(self,naturalidade):
        self.naturalidade = naturalidade

class Aluno(Pessoa):
    def __init__(self, escolaridade, naturalidade):
        Pessoa.__init__(escolaridade, naturalidade)

class Professor(Pessoa):
    def __init__(self, escolaridade, naturalidade):
        Pessoa.__init__(self,escolaridade, naturalidade)

class Curso:
    def __init__(self,coordenador):
        self.coordenador = coordenador

    def getEscolaridadeCoordenador(self):
        return self.coordenador.getDescricãoEscolaridade()    

class Escola:
    def __init__(self,diretor):
        self.diretor = diretor

    def getEscolaridadeDiretor(self):
        return self.diretor.getDescricãoEscolaridade()


#A
print("Letra a:" + " " + "escolaridade de um professor é:")
escolaridade = Escolaridade("NIvel Superior")
professor = Professor(escolaridade, None)
print(professor.getDescricãoEscolaridade())

#Letra B
print("Letra b:" + " " + "escolaridade de um coordenador é:")
escolaridade = Escolaridade("NIvel Superior")
professor = Professor(escolaridade, None)
curso = Curso(professor)
print(curso.getEscolaridadeCoordenador())

#Letra C
print("Letra c:" + " " + "escolaridade de um diretor é: ")
escolaridade = Escolaridade("NIvel Superior")
professor = Professor(escolaridade, None)
escola = Escola(professor) 
print(escola.getEscolaridadeDiretor())